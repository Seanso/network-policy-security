# Network Security in OpenShift
Here you will find policies and instraction on how to prevent network policy deletion.

#### Install kyverno

 [Find the official documentation here](https://kyverno.io/docs/installation/)

I used YAML files, but it's also possible to use helm charts.
```
kubectl create -f https://raw.githubusercontent.com/kyverno/kyverno/main/config/release/install.yaml
```

#### Network Policy
Create a network policy to allow communication from your Nginx namespace to your application namespace. You can also use tags to allow traffic from/to specific pods.

```
oc apply -f from-nginx-to-project-network-policy.yaml
```

#### Add Network Policy
Use the following network policy to force the creation of a "deny all" network policy in all new projects.

```
oc apply -f add-network-policy.yaml
```

#### Prevent network policy deletion or update
Using this policy, only cluster admins will be able to delete or update the "deny all" network policy that we created in the previous step.

```
oc apply -f disallow-network-policy-deletion.yaml
```
