# Network Security in OpenShift


#### Modify Gatekeeper configuration
To Enable Validation of Delete Operations follow [this guide](https://open-policy-agent.github.io/gatekeeper/website/docs/customize-admission/#enable-delete-operations)


#### Add Network Policy
Apply the template and the constaraint:
```
oc apply -f open-policy-agent/template.yaml
oc apply -f open-policy-agent/constraint.yaml
```

Try deleting/editing the default-deny network policy and make sure you get an error.
